# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

All [Unreleased] changes can be viewed in GitLab.

## [2.0.1] - 2024-01-26

### Tech Debt
- Java 11 required

## [2.0.0] - 2024-01-24

### Changed
- Output Git version as well as resolved Java version in `version` task
- Use Plugin API to apply plugin tasks

### Upgrades
- Migrated to latest Gradle groovy plugins
- Gradle 8.4 upgrade

## [1.0.0] - 2024-01-24

### Added
- Automatic versioning capability for basic x.y.z tags
- Automatic increment/SNAPSHOT for intermediate versions
- Handles minor version RC/Alpha/Beta tags (x.y-rcz, x.y-alphaz, x.y-betaz)

[Unreleased]: https://gitlab.com/deltafi/deltafi/-/compare/2.0.0...main
[2.0.0]: https://gitlab.com/deltafi/deltafi/-/compare/1.0.0...2.0.0
[1.0.0]: https://gitlab.com/deltafi/deltafi/-/compare/0.0.5...1.0.0
