package org.deltafi
/**
 *    DeltaFi - Data transformation and enrichment platform
 *
 *    Copyright 2021-2023 DeltaFi Contributors <deltafi@deltafi.org>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
import org.gradle.api.Plugin
import org.gradle.api.Project
class Versioner implements Plugin<Project> {

    static String reckonedVersion
    static String gitDescribeVersion

    static String gitVersion() {
        def sout = new StringBuilder()
        def serr = new StringBuilder()
        def proc = 'git describe --tags'.execute()

        proc.waitForProcessOutput(sout, serr)

        String result = sout
        gitDescribeVersion = result?.trim()
        if (!result?.trim()) return "0.0.0-SNAPSHOT"
        return result.trim()
    }

    static String reckon() {
        reckonedVersion = gitVersion()

        def rcMatcher = reckonedVersion =~ /(\d+)\.(\d+)-(alpha|beta|rc)(\d*)-.*/
        if (rcMatcher.size() > 0) {
            def major = rcMatcher[0][1]
            def minor = rcMatcher[0][2]
            def rc = rcMatcher[0][3]
            def num = 0
            if (rcMatcher[0][4] != "" ) num = rcMatcher[0][4] as Integer
            num++
            reckonedVersion = major + "." + minor + "-" + rc + num + "-SNAPSHOT"
            return reckonedVersion
        }

        def snapshotMatcher = reckonedVersion =~ /(\d+)\.(\d+)\.(\d+)-(\d+)-([0-9a-z]+).*/
        if (snapshotMatcher.size() > 0) {
            def major = snapshotMatcher[0][1]
            def minor = snapshotMatcher[0][2] as Integer
            def patch = snapshotMatcher[0][3] as Integer
            patch++
            reckonedVersion = major + "." + minor + "." + patch + "-SNAPSHOT"
        }

        return reckonedVersion
    }

    @Override
    void apply(Project project) {
        reckon()
        project.allprojects {
            version = Versioner.reckonedVersion
        }
        project.tasks.create("version") {
            doLast {
                println ""
                println "Git version is: " + Versioner.gitDescribeVersion
                println "Resolved Java version is: " + Versioner.reckonedVersion
                println ""
            }
        }
    }
}
